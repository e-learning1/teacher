import logger from "redux-logger";
import { createStore, compose, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import rootReducers from "./reducer";
import rootSaga from "./saga";

let middleware = [];
const sagaMiddleware = createSagaMiddleware();
if (process.env.NODE_ENV === "development") middleware.push(logger);
const store = createStore(
  rootReducers,
  compose(applyMiddleware(sagaMiddleware, ...middleware))
);

rootSaga.forEach(sg => {
  sagaMiddleware.run(sg);
});

export default store;
