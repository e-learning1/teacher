import { combineReducers } from "redux";
import LoginReducer from '../pages/Login/Reducer';

// root reducer
const appReducer = combineReducers({
 ...LoginReducer,
});
const intialReducer = appReducer({}, {});

const rootReducer = (state, action) => {
  if (action.type === "LOGOUT") state = intialReducer;
  return appReducer(state, action);
};

export default rootReducer;
