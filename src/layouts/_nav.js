import Dashboard from '../pages/Dashboard/Component';
import Course from '../pages/Course/Component';
import Profile from '../pages/Profile/Container';


export default [
	{
		path: ['/app/dashboard', '/'],
		key: '/app/dashboard',
		icon: 'dashboard',
		title: 'Bảng Điều Khiển',
		component: Dashboard,
		exact: true,
		isMenu: true,
		isMobile: false,
	},
	{
		path: '/app/course',
		key: '/app/course',
		icon: 'read',
		title: 'Khóa Học',
		component: Course,
		exact: true,
		isMenu: true,
		isMobile: false,
	},
	{
		path: '/app/profile',
		key: '/app/profile',
		icon: 'setting',
		title: 'Quản lý tài khoản',
		component: Profile,
		exact: true,
		isMenu: false,
		isMobile: false,
	},
];
