import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Dropdown, Menu, Avatar } from 'antd';
import { useHistory } from 'react-router-dom';

import { HeaderCustom, GroupMenuHeader } from './Layout.styled';

function HeaderComponent(props) {
	const { setCollapsed, collapsed, handleLogout, userData } = props;
	const history = useHistory();

	const menu = (
		<Menu>
			<Menu.Item>
				<a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">
					1st menu item
				</a>
			</Menu.Item>
			<Menu.Item>
				<a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">
					2nd menu item
				</a>
			</Menu.Item>
			<Menu.Item>
				<a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">
					3rd menu item
				</a>
			</Menu.Item>
		</Menu>
	);

	return (
		<HeaderCustom resize={collapsed ? 'collapsed' : 'undefind'}>
			<div>
				<Icon
					className="trigger"
					type={collapsed ? 'menu-unfold' : 'menu-fold'}
					onClick={() => setCollapsed(!collapsed)}
				/>
				<GroupMenuHeader>
					<span className="phh-menuNoti">
						<Dropdown placement="bottomRight" overlay={menu}>
							<Icon type="bell" />
						</Dropdown>
					</span>
					<span className="phh-menuAcc">
						<Dropdown
							placement="bottomRight"
							overlay={
								<Menu>
									<Menu.Item>
										<p>
											Xin chào <br /> {userData && userData.email} !
										</p>
									</Menu.Item>
									<Menu.Item onClick={() => history.push('/app/profile')}>
										<Icon type="user" />
										Quản lý tài khoản
									</Menu.Item>
									<Menu.Item onClick={handleLogout}>
										<Icon type="logout" />
										Đăng xuất
									</Menu.Item>
								</Menu>
							}
						>
							<Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
						</Dropdown>
					</span>
				</GroupMenuHeader>
			</div>
		</HeaderCustom>
	);
}

HeaderComponent.propTypes = {
	setCollapsed: PropTypes.func.isRequired,
	handleLogout: PropTypes.func.isRequired,
	collapsed: PropTypes.bool.isRequired,
	userData: PropTypes.object.isRequired,
};

export default HeaderComponent;
