import React, { useState, useEffect } from 'react';
import { useHistory, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Spin, message } from 'antd';

// other
import routes from './_nav';
import PrivateRoute from '../utils/PrivateRoute';
import { LayoutCustom, LayoutMain, ContentCustom } from './Layout.styled';
import LoginAction from '../pages/Login/Action';
// import Loading from '../utils/loading';
// component
import Header from './Header';
import Sidebar from './Sidebar';
import SidebarMobile from './SidebarMobile';

function LayoutAdmin(props) {
	const { statusFetch, userData, getProfileReq } = props;

	const [collapsed, setCollapsed] = useState(false);
	const [clientWidth, setClientWidth] = useState(window.document.body.clientWidth);
	const history = useHistory();
	
	useEffect(() => {
	
			getProfileReq({
				req: {},
				cb: res => {
					if (res) {
						localStorage.clear();
						history.push('/login');
					}
				},
			});
		

		const handleSize = () => setClientWidth(window.document.body.clientWidth);
		window.addEventListener('resize', handleSize);
		return () => window.removeEventListener('resize', handleSize);
	}, [getProfileReq,history]);


	const onChangePage = location => {
		history.push(`${location}`);
	};
	const handleLogout = () => {
		localStorage.clear();
		history.push('/login');
		message.success('Đăng xuất thành công');
	};

	const handleRenderRoute = () => {
		if (routes.length > 0) {
			return routes.map(route => {
				if (route.children && route.children.length > 0) {
					return route.children.map(routeChild => (
						<PrivateRoute
							key={routeChild.key}
							exact={routeChild.exact}
							path={routeChild.path}
							component={routeChild.component}
							authority={true}
						/>
					));
				}
				return (
					<PrivateRoute
						authority={true}
						key={route.key}
						exact={route.exact}
						path={route.path}
						component={route.component}
					/>
				);
			});
		}
	};
	return (
		<Spin spinning={statusFetch === 'FETCHING'}>
			<LayoutCustom>
				{clientWidth < 445 ? (
					<SidebarMobile collapsed={collapsed} setCollapsed={setCollapsed} onChangePage={onChangePage} />
				) : (
					<Sidebar onChangePage={onChangePage} collapsed={collapsed} />
				)}
				<LayoutMain resize={collapsed ? 'collapsed' : 'undefind'}>
					<Header collapsed={collapsed} setCollapsed={setCollapsed} handleLogout={handleLogout} userData={userData} />
					<ContentCustom>
						<Switch>{handleRenderRoute()}</Switch>
					</ContentCustom>
				</LayoutMain>
			</LayoutCustom>
		</Spin>
	);
}

const mapStateToProps = state => ({
	userData: state.loginPage.userData,
	statusFetch: state.loginPage.statusFetch,
});

const mapDispatchToProps = {
	getProfileReq: LoginAction.getProfileRequest,
};

LayoutAdmin.propTypes = {
	getProfileReq: PropTypes.func.isRequired,
	userData: PropTypes.object.isRequired,
	statusFetch: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(LayoutAdmin);
