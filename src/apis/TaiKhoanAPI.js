import restful from '../utils/callAPI';

const login = async req => {
	try {
		return await restful.POST('/login/admin', req);
	} catch (err) {
		return err.response.data;
	}
};

const getProfile = async req => {
	try {
		return await restful.GET('/admin/get-profile', req);
	} catch (err) {
		return err.response.data;
	}
};



export default {
	login,
	getProfile,
};
