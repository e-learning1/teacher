
import LoginComponent from '../pages/Login/Container';
import AdminLayout from '../layouts/LayoutAdmin';


export default [
  {
    isPrivate:false,
    isMain:false,
    path:'/login',
    exact:true,
    component:LoginComponent
  },
  {
    isPrivate:false,
    isMain:false,
    path:'/forget-password',
    exact:true,
    component:LoginComponent
  },
  {
    isPrivate:true,
    isMain:true,
    path:'/app/dashboard',
    exact:false,
    component:AdminLayout
  },
 

];