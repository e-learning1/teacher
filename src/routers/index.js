import React from 'react';
import PrivateRoute from '../utils/PrivateRoute';
import { BrowserRouter, Switch } from 'react-router-dom';
import routes from './routers';

const showRoutes = routes => {
	let result = null;
	if (routes.length > 0) {
		result = routes.map((route, index) => {
			if (route.isMain && route.isPrivate)
				return (
					<PrivateRoute
						authority={true}
						key={index}
						exact={route.exact}
						path={[route.path, '/']}
						component={route.component}
					/>
				);
			if (route.isPrivate && !route.isPrivate) {
				return (
					<PrivateRoute
						authority={true}
						key={index}
						exact={route.exact}
						path={route.path}
						component={route.component}
					/>
				);
			}
			return (
				<PrivateRoute authority={false} key={index} exact={route.exact} path={route.path} component={route.component} />
			);
		});
	}
	return result;
};

export default function Routers() {
	return (
		<BrowserRouter>
			<Switch>{showRoutes(routes)}</Switch>
		</BrowserRouter>
	);
}
