import Redux from '../../utils/redux';

const { createAsyncAction } = Redux;

const { loginRequest, loginSuccess, loginFailure } = createAsyncAction('login', 'LOGIN');
const { logoutRequest, logoutSuccess, logoutFailure } = createAsyncAction('logout', 'LOGOUT');
const { getProfileRequest, getProfileSuccess, getProfileFailure } = createAsyncAction('getProfile', 'GET_PROFILE');


const Actions = {
  loginRequest,
  loginSuccess,
  loginFailure,

  logoutRequest,
  logoutSuccess,
  logoutFailure,

  getProfileRequest,
  getProfileSuccess,
  getProfileFailure
};
export default Actions;
