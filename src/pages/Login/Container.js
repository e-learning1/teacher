import {connect} from 'react-redux';

import Component from './Component';
import LoginAction from './Action';

const mapStateToProps = (state) => ({
  statusLogin:state.loginPage.statusLogin,
});

const mapActionToProps = {
  loginRequest:LoginAction.loginRequest,
};



export default connect(mapStateToProps,mapActionToProps)(Component);
