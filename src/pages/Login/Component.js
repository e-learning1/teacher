import React from 'react';
import { Row, Col, Form, Input, Icon, notification } from 'antd';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
// styled
import { LoginContainer, LoginButton, Container } from './Login.styled';

function LoginComponent(props) {
	const { form, statusLogin, loginRequest } = props;

	const loadingLogin = statusLogin === 'FETCHING';

	const history = useHistory();

	const handleSubmit = e => {
		e.preventDefault();
		form.validateFields((err, values) => {
			if (!err) {
				loginRequest({
					req: {
						...values,
					},
					cb: res => {
						if (res) {
							history.push('/app/dashboard');
							notification.success({
								message: 'Thông báo !',
								description:
									'Đăng nhập thành công',
								placement:'bottomRight'
							});
						}
					},
				});
			}
		});
	};

	return (
		<LoginContainer>
			<Row type="flex" justify="center" align="middle">
				<Container>
					<Form className="phh-form-login" onSubmit={handleSubmit}>
						<Col span={24}>
							<div className="logo">
								<h1>ACADEMY</h1>
								HCM
							</div>
						</Col>
						<Col span={24}>
							<label>Email</label>
							<Form.Item>
								{form.getFieldDecorator('email', {
									rules: [
										{
											required: true,
											message: 'Không được để trống Email',
										},
										{
											type: 'email',
											message: 'Email không hợp lệ',
										},
										{
											whitespace: true,
											message: 'Chứa ký tự không hợp lệ',
										},
									],
								})(<Input type="email" prefix={<Icon type="mail" />} placeholder="Nhập email" />)}
							</Form.Item>
						</Col>
						<Col span={24}>
							<label>Mật khẩu</label>
							<Form.Item>
								{form.getFieldDecorator('password', {
									rules: [
										{
											required: true,
											message: 'Không được để trống mật khẩu',
										},
										{
											whitespace: true,
											message: 'Chứa ký tự không hợp lệ',
										},
									],
								})(<Input.Password prefix={<Icon type="lock" />} placeholder="Nhập mật khẩu" />)}
							</Form.Item>
						</Col>
						<Col span={24}>
							<Form.Item>
								<LoginButton loading={loadingLogin} htmlType="submit">
									ĐĂNG NHẬP
								</LoginButton>
							</Form.Item>
						</Col>
					</Form>
				</Container>
			</Row>
		</LoginContainer>
	);
}

LoginComponent.propTypes = {
	form: PropTypes.any.isRequired,
	loginRequest: PropTypes.func.isRequired,
	statusLogin: PropTypes.string.isRequired,
};

export default Form.create({ name: 'phh-login' })(LoginComponent);
