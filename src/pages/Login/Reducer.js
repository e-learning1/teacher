import { STATUS } from "../../constands/Status";
import Action from "./Action";
import Redux from "../../utils/redux";

const { createReducers } = Redux;

const initialState = {
  statusLogin: STATUS.DEFAULT,
  statusLogout: STATUS.DEFAULT,
  statusFetch: STATUS.DEFAULT,
  userData: {}
};

const reducer = [
  // active when call action login
  {
    on: Action.loginRequest,
    reducer: state => ({
      ...state,
      statusLogin: STATUS.FETCHING
    })
  },
  {
    on: Action.loginSuccess,
    reducer: state => ({
      ...state,
      statusLogin: STATUS.SUCCESS,
      isLogin: true,
    })
  },
  {
    on: Action.loginFailure,
    reducer: state => ({
      ...state,
      statusLogin: STATUS.FAILURE
    })
  },
  // active when call action logout
  {
    on: Action.logoutRequest,
    reducer: state => ({
      ...state,
      statusLogout: STATUS.FETCHING
    })
  },
  {
    on: Action.logoutSuccess,
    reducer: state => ({
      ...state,
      statusLogout: STATUS.SUCCESS,
      isLogin:false,
    })
  },
  {
    on: Action.logoutFailure,
    reducer: state => ({
      ...state,
      statusLogout: STATUS.FAILURE
    })
  },
// active when call action get profile
  {
    on: Action.getProfileRequest,
    reducer: state => ({
      ...state,
      statusFetch: STATUS.FETCHING
    })
  },
  {
    on: Action.getProfileSuccess,
    reducer: (state, action) => {
      const { payload } = action;
      return {
        ...state,
        statusFetch: STATUS.SUCCESS,
        userData: payload
      };
    }
  },
  {
    on: Action.getProfileFailure,
    reducer: state => ({
      ...state,
      statusFetch: STATUS.FAILURE
    })
  }
];
export default createReducers("loginPage", reducer, initialState);
