import styled from 'styled-components';
import { Button } from 'antd';

// #####  login container
export const LoginContainer = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-item: center;
  background-color: #43525a;
`;
// ##### end loging container

export const Container = styled.div`
  background-color: #fff;
  border-radius: 3px;
  width: 450px;
  box-shadow: rgb(144, 135, 135) -2px 2px 20px 0px;
  .logo {
    display: flex;
    justify-content: center;
    width: 100%;
    height: auto;
    h1 {
      color: rgb(0, 189, 135);
      font-weight: bold;
    }
  }
  .phh-form-login {
    padding: 3em;
    label {
      font-size: 15px;
      font-weight: 600;
      margin-bottom: 5px;
      display: block;
    }
    input {
      height: 50px;
      font-size: 16px;
    }
    .ant-input-suffix{
      i{
        font-size:15px;
      }
    }
  }
`;

export const LoginButton = styled(Button)`
  display: block;
  margin: auto;
  width: 100%;
  height: 50px;
  color: #fff;
  margin-top:10px;
  &:hover, &:active, &:focus {
    background-color: rgb(0, 189, 135) !important;
    color: #fff;
    filter: brightness(115%);
  }
  background-color: rgb(0, 189, 135);
`;
