import { call, put } from 'redux-saga/effects';
import { message } from 'antd';
import Redux from '../../utils/redux';
import LoginAction from './Action';
import AccountApi from '../../apis/TaiKhoanAPI';
import filterError from '../../utils/filterError';

const { createSagas } = Redux;

function* hanldeLogin(action) {
  try {
    const { req, cb } = action.payload;
    let res = yield call(AccountApi.login, req);
    if (!res.error) {
      yield put(LoginAction.loginSuccess());
      yield localStorage.setItem('token', res.result.token);
      if(cb && typeof cb === 'function') yield cb(true);
    //  yield put(LoginAction.fetchMeRequest({}));
    } else {
      yield put(LoginAction.loginFailure());
      filterError(res.error, 'message');
    }
  } catch (error) {
    yield put(LoginAction.loginFailure());
    message.error('Đăng nhập không thành công ! Xin thử lại');
  }
}
function* hanldeLogout() {
  try {
      yield put(LoginAction.logoutSuccess());
      yield localStorage.clear();
      window.location.href('/login');
  } catch (error) {
    yield put(LoginAction.logoutFailure());
    message.error('Đăng xuất không thành công ! Xin thử lại');
  }
}
function* handleFetch(action) {
  try {
    const {req, cb} = action.payload;
    let res = yield call(AccountApi.getProfile,req);
    if (!res.error) {
      yield put(LoginAction.getProfileSuccess(res.result));
      yield localStorage.setItem('role', res.result.role);
    } else {
      yield put(LoginAction.getProfileFailure());
      if(cb && typeof cb === 'function') yield cb(true);
      filterError(res.error, 'message');
    }
  } catch (error) {
    yield put(LoginAction.getProfileFailure());
    message.error('Lấy thông tin người dùng không thành công ! Xin thử lại');
  }
}

const loginSaga = {
  on: LoginAction.loginRequest,
  worker: hanldeLogin
};

const logoutSaga = {
  on: LoginAction.logoutRequest,
  worker: hanldeLogout
};

const fetchSaga = {
  on: LoginAction.getProfileRequest,
  worker: handleFetch
};
export default createSagas([loginSaga, logoutSaga, fetchSaga]);
