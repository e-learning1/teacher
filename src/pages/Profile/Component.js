import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Card, PageHeader, Form, Input, Button, Tree, Icon } from 'antd';

const { TreeNode } = Tree;

function Component(props) {
	const { form, profile } = props;

	return (
		<div>
			<div className="phh-page-header">
				<Row>
					<Col xs={24} sm={12} md={12}>
						<PageHeader title="Quản lý tài khoản" />
					</Col>
				</Row>
			</div>
			<div className="phh-padding-1em">
				<Row gutter={16}>
					<Col xs={24} sm={12} md={16} className="phh-margin-bottom-15">
						<Card className="phh-card" title="Thông tin cá nhân">
							<Form className="update-profile">
								<Form.Item>
									{form.getFieldDecorator('email', {
										initialValue: profile.email,
										rules: [
											{
												required: true,
												message: 'Không được để trống Email',
											},
											{
												type: 'email',
												message: 'Email không hợp lệ',
											},
											{
												whitespace: true,
												message: 'Chứa ký tự không hợp lệ',
											},
										],
									})(<Input type="email" prefix={<Icon type="mail" />} />)}
								</Form.Item>
								<Form.Item label="Họ tên" labelAlign="left">
									{form.getFieldDecorator('name', {
										initialValue: profile.name,
										rules: [
											{
												required: true,
												message: 'Không được để trống họ tên',
											},
										],
									})(<Input name="name" prefix={<Icon type="user" />}  />)}
								</Form.Item>
								<Form.Item label="Số điện thoại" labelAlign="left">
									{form.getFieldDecorator('mobilePhone', {
										initialValue: profile.mobilePhone,
										rules: [
											{
												required: true,
												message: 'Không được để trống điện thoại',
											},
											{
												pattern: /^[0-9]*$/,
												message: 'Input is not number!',
											},
										],
									})(<Input name="mobilePhone" prefix={<Icon type="phone" />}  />)}
								</Form.Item>
								<Form.Item>
									<Button type="primary" style={{ float: 'right' }}>
										Cập nhật
									</Button>
								</Form.Item>
							</Form>
						</Card>
					</Col>
					<Col xs={24} sm={12} md={8} className="phh-margin-bottom-15">
						<Card className="phh-card" title="Quyền">
							<Tree
								checkable
								defaultExpandedKeys={['0-0-0', '0-0-1']}
								defaultSelectedKeys={['0-0-0', '0-0-1']}
								defaultCheckedKeys={['0-0-0', '0-0-1']}
							>
								<TreeNode title="parent 1" key="0-0">
									<TreeNode title="parent 1-0" key="0-0-0" disabled>
										<TreeNode title="leaf" key="0-0-0-0" disableCheckbox />
										<TreeNode title="leaf" key="0-0-0-1" />
									</TreeNode>
									<TreeNode title="parent 1-1" key="0-0-1">
										<TreeNode title={<span style={{ color: '#1890ff' }}>sss</span>} key="0-0-1-0" />
									</TreeNode>
								</TreeNode>
							</Tree>
						</Card>
					</Col>
				</Row>
			</div>
		</div>
	);
}

Component.propTypes = {
	form: PropTypes.any.isRequired,

	profile: PropTypes.object.isRequired,
};

export default Form.create({ name: 'update-profile' })(Component);
