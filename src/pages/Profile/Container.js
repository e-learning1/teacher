import {connect} from 'react-redux';

// import SettingAction from './Action';
import Component from './Component';


const mapStateToProps = (state) => ({
  profile:state.loginPage.userData,

});

const mapDispatchToProps = {
 // updateReq:SettingAction
};

export default connect(mapStateToProps,mapDispatchToProps)(Component);