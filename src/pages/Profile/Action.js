import Redux from '../../utils/redux';

const { createAsyncAction } = Redux;

const { updateProfileRequest, updateProfileSuccess, updateProfileFailure } = createAsyncAction(
	'updateProfile',
	'UPDATE_PROFILE',
);

const { getRulesRequest, getRulesSuccess, getRulesFailure } = createAsyncAction('getRules', 'GET_RULES');
const { createRulesRequest, createRulesSuccess, createRulesFailure } = createAsyncAction('createRules', 'GET_RULES');
const { updateRulesRequest, updateRulesSuccess, updateRulesFailure } = createAsyncAction('updateRules', 'UPDATE_RULES');
const { deleteRulesRequest, deleteRulesSuccess, deleteRulesFailure } = createAsyncAction('deleteRules', 'DELETE_RULES');

const { getSubAdminRequest, getSubAdminSuccess, getSubAdminFailure } = createAsyncAction(
	'getSubAdmin',
	'GET_SUB_ADMIN',
);

const { searchSubAdminRequest, searchSubAdminSuccess, searchSubAdminFailure } = createAsyncAction(
	'searchSubAdmin',
	'SEARCH_SUB_ADMIN',
);

const { createSubAdminRequest, createSubAdminSuccess, createSubAdminFailure } = createAsyncAction(
	'createSubAdmin',
	'CREATE_SUB_ADMIN',
);

const { updateSubAdminRequest, updateSubAdminSuccess, updateSubAdminFailure } = createAsyncAction(
	'updateSubAdmin',
	'UPDATE_SUB_ADMIN',
);

const { deleteSubAdminRequest, deleteSubAdminSuccess, deleteSubAdminFailure } = createAsyncAction(
	'deleteSubAdmin',
	'DELETE_SUB_ADMIN',
);

const Actions = {
	getRulesRequest,
	getRulesSuccess,
	getRulesFailure,

	createRulesRequest,
	createRulesSuccess,
	createRulesFailure,
	updateRulesRequest,
	updateRulesSuccess,
	updateRulesFailure,

	deleteRulesRequest,
	deleteRulesSuccess,
	deleteRulesFailure,

	updateProfileRequest,
	updateProfileSuccess,
	updateProfileFailure,

	getSubAdminRequest,
	getSubAdminSuccess,
	getSubAdminFailure,

	searchSubAdminRequest,
	searchSubAdminSuccess,
	searchSubAdminFailure,

	createSubAdminRequest,
	createSubAdminSuccess,
	createSubAdminFailure,

	updateSubAdminRequest,
	updateSubAdminSuccess,
	updateSubAdminFailure,

	deleteSubAdminRequest,
	deleteSubAdminSuccess,
	deleteSubAdminFailure,
};

export default Actions;
