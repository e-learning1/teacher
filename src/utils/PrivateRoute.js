import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

function PrivateRoute({ component: Component, authority, ...rest }) {
	const token = localStorage.getItem('token');
	return authority ? (
		<Route
			{...rest}
			render={routeProp => (token ? <Component {...routeProp} /> : <Redirect to={{ pathname: '/login' }} />)}
		/>
	) : (
		<Route
			{...rest}
			render={routeProp => (!token ? <Component {...routeProp} /> : <Redirect to={{ pathname: '/app/dashboard' }} />)}
		/>
	);
}

PrivateRoute.propTypes = {
	component: PropTypes.any.isRequired,
	authority:PropTypes.bool.isRequired,
};

export default PrivateRoute;
