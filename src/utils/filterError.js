import { message, notification } from 'antd';
//import {useHistory} from 'react-router-dom';

export default function(data, type) {
	// const history = useHistory();
	if (data.length > 0) {
		if (type && type === 'notification') {
			return data.map(ele =>
				notification.error({
					message: 'Thông báo lỗi',
					description: ele.msg,
					placement: 'topRight',
				}),
			);
		}
		return data.map(ele => {
			if (ele.msg === 'Xác thực không hợp lệ') {
				localStorage.clear();
				window.location.href('/login');
				return message.error(ele.msg);
			}
			return message.error(ele.msg);
		});
	}
}
