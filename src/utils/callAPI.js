import axios from 'axios';
const API_URL = process.env.NODE_ENV === 'production' ? '' : 'http://localhost:8000';

async function callAPI(path, type, payload) {
  try {
    const url = new URL(`${API_URL}${path}`);
    const token = localStorage.getItem('token');
    if (type === 'GET') Object.keys(payload).forEach(key => url.searchParams.append(key, payload[key]));
    let options = {
      method: type,
      headers: {
        'x-access-token': token || '',
        'Content-Type': 'application/json'
      },
      url
    };

    if (type === 'POST' || type === 'PATCH') options.data = payload;
    const res = await axios(options);
    return res.data;
  } catch (err) {
    if (err) throw err;
  }
}
const restful = {
  GET: (path, params) => callAPI(path, 'GET', params),
  POST: (path, body) => callAPI(path, 'POST', body),
  PATCH: (path, body) => callAPI(path, 'PATCH', body),
  DELETE: (path, params) => callAPI(path, 'DELETE', params)
};
export default restful;
