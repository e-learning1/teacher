import { notification, message } from 'antd';

const openNotificationWithIcon = (msg, action) => {
	notification[action]({
		message: 'Thông báo',
		description: msg,
		placement: 'bottomRight',
	});
};

const openMessage = (msg, action) => {
	message[action](msg);
};

export default function(type, action, mess) {
	if (type === 'message') openMessage(mess, action);
	else openNotificationWithIcon(mess, action);
}
